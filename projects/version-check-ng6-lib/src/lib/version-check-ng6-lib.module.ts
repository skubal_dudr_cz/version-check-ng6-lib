import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { VersionCheckNg6LibService } from './version-check-ng6-lib.service';

@NgModule({
  imports: [
    HttpClientModule
  ],
  declarations: [],
  exports: [VersionCheckNg6LibService],
  providers: [HttpClientModule, VersionCheckNg6LibService],
})
export class VersionCheckNg6LibModule { }

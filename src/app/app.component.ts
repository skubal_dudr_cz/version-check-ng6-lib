import { Component, OnInit } from '@angular/core';
import { environment } from '../environments/environment';
import { VersionCheckNg6LibService } from 'version-check-ng6-lib';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'version-check-ng6-lib-app';

  versionCheckFrequency = 1000 * 60 * 10; // every 10 minutes

  constructor(
    private versionCheckService: VersionCheckNg6LibService
  ) {}

  ngOnInit(): void {
    if (environment.production) {
      this.versionCheckService.initVersionCheck(
        '/version.json',
        this.versionCheckFrequency,
        this.newVersionDetected
      );
    }
  }

  private newVersionDetected(hash: string): void {
    console.log('New version has been detected, reloading…', hash);
    location.reload();
  }
}
